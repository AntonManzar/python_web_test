from flask import Flask, render_template, request
from DBcm import UseDatabase, ConnecnionError, CredentialsError, SQLError

app = Flask(__name__)
app.config['dbconfig'] = {'dbname': 'postgres',
                          'user': 'anton',
                          'host': 'localhost',
                          'password': '********',
                          'port': 5432}


def log_request(req: 'flask_request', res: str) -> None:
    with UseDatabase(app.config['dbconfig']) as cursor:
        _SQL = '''insert into log
                  (phrase, letters, ip, browser_string, results) 
                  values (%s, %s, %s, %s, %s)'''
        data = (req.form['phrase'],
                req.form['letters'],
                req.remote_addr,
                req.user_agent.string,
                res,)
        cursor.execute(_SQL, data)


@app.route('/search', methods=['POST'])
def search() -> 'html':
    phrase = request.form['phrase']
    letters = request.form['letters']
    title = 'Here are you result'
    results = str(set(letters).intersection(set(phrase)))
    try:
        log_request(request, results)
    except Exception as error:
        print('******* Logging failed:', str(error))
    return render_template('results.html', the_title=title, the_result=results, the_phrase=phrase, the_letters=letters)


@app.route('/')
@app.route('/entry')
def entry_page() -> 'html':
    return render_template('entry.html', the_title='Welcome to search on the web')


@app.route('/viewlog')
def veiw_the_log() -> 'html':
    try:
        with UseDatabase(app.config['dbconfig']) as cursor:
            _SQL = '''select phrase, letters, ip, browser_string, results from log'''
            cursor.execute(_SQL)
            contents = cursor.fetchall()
        titles = ('Phrase', 'Letters', 'Remote Addr', 'User Agent', 'Results')
        return render_template('viewlog.html', the_title='View Log', the_row_titles=titles, the_data=contents, )
    except ConnecnionError as error:
        print('Is your database switched on?:', str(error))
    except CredentialsError as error:
        print('User - id/password issure. Error,s: ', str(error))
    except SQLError as error:
        print('Is your query correct?:', str(error))
    except Exception as error:
        print('Something went wrong:', str(error))
    return 'Error'


app.secret_key = 'YouWillNeverGuess...'

if __name__ == '__main__':
    app.run(debug=True)
