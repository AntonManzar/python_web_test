import psycopg2


class ConnecnionError(Exception):
    pass


class CredentialsError(Exception):
    pass


class SQLError(Exception):
    pass


class UseDatabase:

    def __init__(self, dbconfig: dict) -> None:
        self.dbconfig = dbconfig

    def __enter__(self) -> 'cursor':
        try:
            self.conn = psycopg2.connect(**self.dbconfig)
            self.cursor = self.conn.cursor()
            return self.cursor
        except psycopg2.InterfaceError as error:
            raise ConnecnionError(error)
        except psycopg2.ProgrammingError as error:
            raise CredentialsError(error)

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.conn.commit()
        self.cursor.close()
        self.conn.close()
        if exc_type is psycopg2.ProgrammingError:
            raise SQLError(exc_val)
        elif exc_type:
            raise exc_type(exc_val)
