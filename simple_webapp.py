from flask import Flask, session, render_template
from checker import check_logged_in

app = Flask(__name__)
app.secret_key = 'YouWillNeverGuess...'


@app.route('/')
def index() -> 'html':
    return render_template('ref.html', the_status='Начальная')


@app.route('/login')
def login() -> 'html':
    session['logged_in'] = True
    return render_template('ref.html', the_status='Залогинился')


@app.route('/logout')
def logout() -> 'html':
    session.pop('logged_in')
    return render_template('ref.html', the_status='Разлогинился')


@app.route('/page1')
@check_logged_in
def page1() -> 'html':
    return render_template('ref.html', the_status='Статус1: ' + str("logged_in" in session))


@app.route('/page2')
@check_logged_in
def page2() -> 'html':
    return render_template('ref.html', the_status='Статус2: ' + str("logged_in" in session))


@app.route('/page3')
@check_logged_in
def page3() -> 'html':
    return render_template('ref.html', the_status='Статус3: ' + str("logged_in" in session))


if __name__ == '__main__':
    app.run(debug=True)
